#pragma once
#include<iostream>
#include<WinSock2.h>
#include<Windows.h>
using namespace std;
#pragma comment(lib,"ws2_32.lib")

#define PORT 5188
#define IP_ADDRESS "118.89.103.55"
class winudp
{
	public:

	WSADATA Ws;
	SOCKET sock;//与服务器socket
	struct sockaddr_in serveraddr;//服务端地址
	char sendbuf[1024] = { 0 };//发送缓冲
	char recvbuf[1024] = { 0 };//接收缓冲
	int clie;//客户端
	int akplaygrabbed;//a是否按下
	int bkplaygrabbed;//b是否按下
	float atouchx, atouchy;//a触摸坐标
	float btouchx, btouchy;//b触摸坐标
	float ax, ay;//物体a x,y坐标
	float bx, by;//物体b x,y坐标
	int asizex, asizey;//a图片大小
	int bsizex, bsizey;//b图片大小
	int agoalx, agoaly;//a球门大小
	int bgoalx, bgoaly;//b球门大小
	int ascore;//a得分
	int bscore;//b得分
	float dx, dy;//道具坐标

	winudp(int i)
	{
		if (WSAStartup(MAKEWORD(2, 2), &Ws) != 0)
		{
			cout << "Init Windows Socket Failed::" << GetLastError() << endl;
		}
		sock = socket(PF_INET, SOCK_DGRAM, IPPROTO_UDP);
		if (sock == INVALID_SOCKET)
		{
			cout << "Create Socket Failed::" << GetLastError() << endl;
		}
		memset(serveraddr.sin_zero, 0, sizeof(serveraddr));
		memset(sendbuf, 0, sizeof(sendbuf));
		memset(recvbuf, 0, sizeof(recvbuf));
		serveraddr.sin_family = AF_INET;
		serveraddr.sin_addr.s_addr = inet_addr(IP_ADDRESS);
		if (i == 1)
		{
			serveraddr.sin_port = htons(PORT);
			clie = 1;
		}
		else
		{
			serveraddr.sin_port = htons(5189);
			clie = 2;
		}
		connect(sock, (struct sockaddr*)&serveraddr, sizeof(serveraddr));
		unsigned long ul = 1;
		ioctlsocket(sock, FIONBIO, (unsigned long *)&ul);//设置非阻塞
		//默认值（需要修改）
		ax = 1;
		ay = 1;
		atouchx = 1;
		atouchy = 1;
		agoalx = 1;
		agoaly = 1;
		ascore = 1;
		dx = 1;
		dy = 1;
		asizex = 1;
		asizey = 1;
		akplaygrabbed = 1;
	}
	int send();//发送缓冲区数据
	int recv();//接收数据到缓冲区
	void setmem();//清空
	void setsend(char*);//设置发送数据
	char* readrecv();//读取recvbuf
	void print();
	void open();//开始发送
	void close();//结束发送并关闭socket
};
int winudp::send()
{
	memset(sendbuf, 0, sizeof(sendbuf));
	//序列化
	sprintf(sendbuf, "%f %f %d %f %f %d %d %d %d %f %f %d %f %f %d %f %f %d %d %d %d %d", ax, ay, akplaygrabbed, atouchx, atouchy, agoalx, agoaly, asizex,asizey, dx, dy, ascore, bx, by, bkplaygrabbed, btouchx, btouchy, bgoalx, bgoaly, bsizex, bsizey, bscore);
	sendto(sock, sendbuf, sizeof(sendbuf), 0, (struct sockaddr*)&serveraddr, sizeof(serveraddr));
	return 1;
}
int winudp::recv()
{
	memset(recvbuf, 0, sizeof(recvbuf));
	if(recvfrom(sock, recvbuf, sizeof(recvbuf), 0, NULL, NULL)>=0);
	//反序列化
	{
		sscanf(recvbuf, "%f %f %d %f %f %d %d %d %d %f %f %d %f %f %d %f %f %d %d %d %d %d", ax, ay, akplaygrabbed, atouchx, atouchy, agoalx, agoaly, asizex, asizey, dx, dy, ascore, bx, by, bkplaygrabbed, btouchx, btouchy, bgoalx, bgoaly, bsizex, bsizey, bscore);
	}
	return 1;
}
void winudp::setmem()
{
	memset(sendbuf, 0, sizeof(sendbuf));
	memset(recvbuf, 0, sizeof(recvbuf));
}
void winudp::print()
{
	cout << sendbuf << endl << recvbuf << endl;
	cout << ax << "\t" << ay << "\t" << bscore << endl;
}
void winudp::setsend(char* a)
{
	cin >> sendbuf;
	//sendbuf=a;
}
char* winudp::readrecv()
{
	return recvbuf;
}
void winudp::open()
{
	send();
	recv();
	print();
	Sleep(100);
}
void winudp::close()
{
	closesocket(sock);
}
